public class DuplicateZeros {

    public static void main(String[] args) {
        int[] arr = {1, 0, 2, 3, 0, 4, 5, 0};

        duplicateZeros(arr);

        // Print the modified array
        for (int num : arr) {
            System.out.print(num + " ");
        }
    }

    public static void duplicateZeros(int[] arr) {
        int n = arr.length;
        int[] tempArray = new int[n];

        int j = 0; // Pointer for the temporary array

        for (int i = 0; i < n; i++) {
            if (arr[i] == 0) {
                tempArray[j++] = 0;
                if (j < n) { // Check if the temporary array still has space
                    tempArray[j++] = 0;
                }
            } else {
                tempArray[j++] = arr[i];
            }
        }

        // Copy the modified temporary array back to the original array
        for (int i = 0; i < n; i++) {
            arr[i] = tempArray[i];
        }
    }
}
