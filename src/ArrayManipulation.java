public class ArrayManipulation {
    static double sum = 0.0;
    static double max = 0.0;
    public static void main(String[] args) {
        int[] numbers = { 5, 8, 3, 2, 7 };
        String[] names = { "Alice", "Bob", "Charlie", "David" };
        double[] values = new double[4];
        
        String[] reversedNames = new String[4];
        

        values[0] = 1.1111;
        values[1] = 2.2222;
        values[2] = 3.3333;
        values[3] = 4.4444;
        
       


        System.out.println("Using for loop:");
        arrayUsingForLoop(numbers);
        System.out.println();
        System.out.println("Print the elements of the 'names' array using a for-each loop.");
        arrayUsingForEachLoop(names);
        

        System.out.println();
        System.out.println("Calculate and print the sum of all elements in the 'numbers' array.");
        calculateValue(values);

        maximumValue(values);

        // Create a new string array named "reversedNames" with the same length as the "names" array.
        
        int n = reversedNames.length;
        // Fill the "reversedNames" array with the elements of the "names" array in reverse order.
        for (int i = 0 ; i < n/2; i++){
            String temp = names[i];
            reversedNames[i] = names[n - 1 - i ];
            reversedNames[n - 1 - i] = temp;
        }
        // Print the elements of the "reversedNames" array.
        for(String rename : reversedNames){
            System.out.println(rename + " ");
        }


    }
    

    public static void arrayUsingForLoop(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i] + " ");

        }

    }

    public static void arrayUsingForEachLoop(String[] names) {
        for (String i : names) {
            System.out.println(i + " ");
        }

    }
    public static void calculateValue(double[] values){
        for (int i = 0; i < values.length; i++){
            sum += values[i];
            System.out.println(sum);

        }
    }

    public static void maximumValue(double[] values){
        for (int i = 0; i < values.length; i++){
            if(values[i] > max ){
                max = values[i];
            }
            
        }
        System.out.println("Maximum value in the 'values' array is "+max);
 
    }
    
    public static void sortNumber(int[] numbers){
        for (int i = 0; i < numbers.length; i++) {     
            System.out.println(numbers);    
        }    
    }

    
}