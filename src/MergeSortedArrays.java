public class MergeSortedArrays {

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int p1 = m - 1; // Pointer for nums1
        int p2 = n - 1; // Pointer for nums2
        int p3 = m + n - 1; // Pointer for the merged array nums1

        // Merge the arrays from the back, starting from the last valid elements in both arrays.
        while (p1 >= 0 && p2 >= 0) {
            if (nums1[p1] >= nums2[p2]) {
                nums1[p3] = nums1[p1];
                p1--;
            } else {
                nums1[p3] = nums2[p2];
                p2--;
            }
            p3--;
        }

        // If there are still remaining elements in nums2, copy them to nums1.
        while (p2 >= 0) {
            nums1[p3] = nums2[p2];
            p2--;
            p3--;
        }
    }

    public static void main(String[] args) {
        int[] nums1 = {1, 3, 5, 0, 0, 0};
        System.out.println("Original Array Num1");
        for (int num : nums1) System.out.print(num + " ");
        int m = 3;
        int[] nums2 = {2, 4, 6};
        System.out.println("\nOriginal Array Num2");
        for (int num : nums2) System.out.print(num + " ");
        int n = 3;
        System.out.println();
        merge(nums1, m, nums2, n);
        System.out.println("Merged Array");
        // Print the merged array nums1
        for (int num : nums1) System.out.print(num + " ");
        
        // Output: 1 2 3 4 5 6
    }

}
